using System.Collections.Generic;
using System.Threading.Tasks;
using DatingApp.Api.Helpers;
using DatingApp.Api.Models;

namespace DatingApp.Api.Repository
{
    public interface IBaseRepository
    {
         void Add<T>(T entity) where T: class;
         void Remove<T>(T entity) where T: class;
         Task<PagedList<User>> GetUsersAsync(UserParams userParams);
         Task<User> GetUserAsync(int id);
         Task<bool> SaveAll();
         Task<Photo> GetPhoto(int id);
         Task<Photo> GetMainUserPhoto(int userId);
         Task<Like> GetLike(int userId, int recipientId);
         Task<Message> GetMessage(int id);
         Task<PagedList<Message>> GetMessagesForUser(MessageParams messageParams);
         Task<IEnumerable<Message>> GetMessageThread(int userId, int recipientId);
    }
}