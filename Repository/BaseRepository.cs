using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatingApp.Api.Data;
using DatingApp.Api.Helpers;
using DatingApp.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.Api.Repository
{
    public class BaseRepository : IBaseRepository
    {
        private readonly DataContext _context;
        public BaseRepository(DataContext context)
        {
            _context = context;
        }
        
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public async Task<Photo> GetPhoto(int id)
        {
            var photo = await _context.Photos.FirstOrDefaultAsync(p => p.Id == id);
            return photo;
        }

        public async Task<Photo> GetMainUserPhoto(int userId)
        {
            return await _context.Photos.Where(u => u.UserId == userId)
                .FirstOrDefaultAsync(x => x.IsMain);
        }

        public async Task<User> GetUserAsync(int id)
        {
            return await _context.Users.Include(x => x.Photos).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<PagedList<User>> GetUsersAsync(UserParams userParams)
        {
            var users = _context.Users.Include(x => x.Photos)
                .OrderByDescending(u => u.LastActive).AsQueryable();

            users = users.Where(u => u.Id != userParams.UserId);
            users = users.Where(u => u.Gender == userParams.Gender);

            if(userParams.Likers) {
                var userLikerIds = await getLikeUserIds(userParams.UserId, userParams.Likers);
                users = users.Where(u => userLikerIds.Contains(u.Id));
            }

            if(userParams.Likees) {
                var userLikerIds = await getLikeUserIds(userParams.UserId, userParams.Likers);
                users = users.Where(u => userLikerIds.Contains(u.Id));
            }
            
            if(userParams.MinAge != 18 || userParams.MaxAge != 99) {
                DateTime minAge = DateTime.Today.AddYears(-userParams.MaxAge - 1);
                DateTime maxAge = DateTime.Today.AddYears(-userParams.MinAge - 1);

                users = users.Where(u => u.DateOfBirth >= minAge && u.DateOfBirth <= maxAge);
            }

            if(!string.IsNullOrEmpty(userParams.OrderBy)) {
                switch(userParams.OrderBy) {
                    case "created":
                        users = users.OrderByDescending(u => u.Created);
                        break;
                    default:
                        users = users.OrderByDescending(u => u.LastActive);
                        break;
                }
            }

            return await PagedList<User>.CreateAsync(users, userParams.PageNumber, userParams.PageSize);
        }

        public void Remove<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<bool> SaveAll() {
            if(await _context.SaveChangesAsync() > 0)
                return true;
            return false;
        }

        public async Task<Like> GetLike(int userId, int recipientId)
        {
            return await _context.Likes
                .FirstOrDefaultAsync(l => l.LikerId == userId && l.LikeeId == recipientId);
        }

        private async Task<IEnumerable<int>> getLikeUserIds(int userId, bool liker)
        {
            var userEntity = await _context.Users
                .Include(u => u.Likers)
                .Include(u => u.Likees)
                .FirstOrDefaultAsync(u => u.Id == userId);

            if(liker)
                return userEntity.Likers.Where(l => l.LikeeId == userId).Select(l => l.LikerId);
            else
                return userEntity.Likees.Where(l => l.LikerId == userId).Select(l => l.LikeeId);
        }

        public async Task<Message> GetMessage(int id)
        {
            return await _context.Messages.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<PagedList<Message>> GetMessagesForUser(MessageParams messageParams)
        {
            var messagesCollection = _context.Messages
                .Include(m => m.Sender).ThenInclude(u => u.Photos)
                .Include(m => m.Recipient).ThenInclude(u => u.Photos)
                .AsQueryable();

            switch(messageParams.MessageContainer) {
                case "Inbox":
                    messagesCollection = messagesCollection.Where(m => m.RecipientId == messageParams.UserId);
                    break;
                case "Outbox":
                    messagesCollection = messagesCollection.Where(m => m.SenderId == messageParams.UserId);
                    break;
                default:
                    messagesCollection = messagesCollection.Where(m => m.RecipientId == messageParams.UserId && !m.IsRead);
                    break;
            }

            messagesCollection = messagesCollection.OrderByDescending(m => m.MessageSend);

            return await PagedList<Message>.CreateAsync(messagesCollection, messageParams.PageNumber, messageParams.PageSize);
        }

        public async Task<IEnumerable<Message>> GetMessageThread(int userId, int recipientId)
        {
            var messagesCollection = await _context.Messages
                .Include(m => m.Sender).ThenInclude(u => u.Photos)
                .Include(m => m.Recipient).ThenInclude(u => u.Photos)
                .Where(m => m.RecipientId == recipientId && m.SenderId == userId || 
                    m.RecipientId == userId && m.SenderId == recipientId)
                .OrderByDescending(m => m.MessageSend)
                .ToListAsync();

            return messagesCollection;
        }
    }
}