using System.Threading.Tasks;
using DatingApp.Api.Models;

namespace DatingApp.Api.Repository
{
    public interface IAuthRepository
    {
         Task<User> Login(string username, string password);
         Task<User> Register(User user, string password);
         Task<bool> UserExist(string username);
    }
}