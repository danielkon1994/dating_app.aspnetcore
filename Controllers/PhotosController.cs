using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using DatingApp.Api.Dto;
using DatingApp.Api.Helpers;
using DatingApp.Api.Models;
using DatingApp.Api.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace DatingApp.Api.Controllers
{
    [Authorize]
    [Route("api/users/{userId}/photos")]
    public class PhotosController : Controller
    {
        private Cloudinary _cloudinary;
        public IMapper _mapper { get; }
        public IBaseRepository _baseRepository { get; }
        public IOptions<CloudinarySetting> _cloudinarySettings { get; }

        public PhotosController(IBaseRepository baseRepository,
        IMapper mapper,
        IOptions<CloudinarySetting> cloudinarySettings)
        {
            _cloudinarySettings = cloudinarySettings;
            _baseRepository = baseRepository;
            _mapper = mapper;

            Account acc = new Account(
                _cloudinarySettings.Value.CloudName,
                _cloudinarySettings.Value.ApiKey,
                _cloudinarySettings.Value.ApiSecret
            );

            _cloudinary = new Cloudinary(acc);
        }

        [HttpGet("{id}", Name = "GetPhoto")]
        public async Task<IActionResult> GetPhoto(int id) {
            var photoFromRepo = await _baseRepository.GetPhoto(id);
            var photo = _mapper.Map<PhotoForReturnDto>(photoFromRepo);

            return Ok(photo);
        }

        [HttpPost]
        public async Task<IActionResult> AddPhotoForUser(int userId, [FromForm]PhotoForAddDto photoForAddDto)
        {
            if(userId != Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var userFromRepo = await _baseRepository.GetUserAsync(userId);

            var uploadResult = new ImageUploadResult();

            var file = photoForAddDto.File;
            if(file.Length > 0)
            {
                using(var stream = file.OpenReadStream()){
                    var uploadParams = new ImageUploadParams()
                    {
                        File = new FileDescription(file.Name, stream),
                        Transformation = new Transformation()
                            .Width(500)
                            .Height(500)
                            .Crop("fill")
                            .Gravity("face")
                    };

                    uploadResult = _cloudinary.Upload(uploadParams);
                }
            }

            photoForAddDto.Url = uploadResult.Uri.ToString();
            photoForAddDto.PublicId = uploadResult.PublicId;

            var photo = _mapper.Map<Photo>(photoForAddDto);
            if(!userFromRepo.Photos.Any(p => p.IsMain))
                photo.IsMain = true;
            userFromRepo.Photos.Add(photo);

            if(await _baseRepository.SaveAll())
            {
                var photoToReturn = _mapper.Map<PhotoForReturnDto>(photo);
                return CreatedAtRoute("GetPhoto", new {id = photo.Id}, photoToReturn);
            }

            return BadRequest("Could not upload photo");
        }

        [HttpPost("{id}/setMain")]
        public async Task<IActionResult> SetMainPhoto(int userId, int id)
        {
            if(userId != Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var userFromRepo = await _baseRepository.GetUserAsync(userId);
            if(!userFromRepo.Photos.Any(p => p.IsMain))
                return Unauthorized();

            var photoFromRepo = await _baseRepository.GetPhoto(id);
            if(photoFromRepo == null)
                return BadRequest("Photo can not be found");

            if(photoFromRepo.IsMain)
                return BadRequest("This is already main photo");

            var currentMainPhoto = await _baseRepository.GetMainUserPhoto(userId);
            currentMainPhoto.IsMain = false;

            photoFromRepo.IsMain = true;

            if(await _baseRepository.SaveAll())
                return NoContent();

            return BadRequest();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePhoto(int userId, int id)
        {
            if(userId != Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var userFromRepo = await _baseRepository.GetUserAsync(userId);
            if(!userFromRepo.Photos.Any(p => p.IsMain))
                return Unauthorized();

            var photoFromRepo = await _baseRepository.GetPhoto(id);
            if(photoFromRepo == null)
                return BadRequest("Photo can not be found");

            if(photoFromRepo.IsMain)
                return BadRequest("Cannot delete your main photo");

            if(photoFromRepo.PublicId != null)
            {
                var deletionParams = new DeletionParams(photoFromRepo.PublicId);
                var result = _cloudinary.Destroy(deletionParams);

                if(result.Result == "ok")
                    _baseRepository.Remove<Photo>(photoFromRepo);
            }
            else
            {
                _baseRepository.Remove<Photo>(photoFromRepo);
            }
            
            if(await _baseRepository.SaveAll())
                return Ok();

            return BadRequest("Failed to delete the photo");
        }
    }
}