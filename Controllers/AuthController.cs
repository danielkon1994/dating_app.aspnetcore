using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DatingApp.Api.Dto;
using DatingApp.Api.Models;
using DatingApp.Api.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace DatingApp.Api.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly IAuthRepository _authRepo;
        private readonly IMapper _mapper;

        public AuthController(IAuthRepository authRepo,
            IMapper mapper)
        {
            _mapper = mapper;
            _authRepo = authRepo;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] UserForLogin model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userFromRepo = await _authRepo.Login(model.Username, model.Password);

            if (userFromRepo == null)
                return Unauthorized();

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("default security key");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] {
                    new Claim(ClaimTypes.NameIdentifier, userFromRepo.Id.ToString()),
                    new Claim(ClaimTypes.Name, userFromRepo.Username)
                }),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha512)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            var user = _mapper.Map<UserForList>(userFromRepo);

            return Ok(new
            {
                token = tokenString,
                user = user
            });
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] UserForRegister model)
        {
            model.Username = model.Username.ToLower();

            if (await _authRepo.UserExist(model.Username))
                ModelState.AddModelError("Username", "User already exists");

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            User user = _mapper.Map<User>(model);

            User createdUser = await _authRepo.Register(user, model.Password);

            UserForDetails userToReturn = _mapper.Map<UserForDetails>(createdUser);

            return CreatedAtRoute("GetUser", new {id = createdUser}, userToReturn);
        }
    }
}