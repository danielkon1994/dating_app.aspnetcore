using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using DatingApp.Api.Data;
using DatingApp.Api.Dto;
using DatingApp.Api.Helpers;
using DatingApp.Api.Models;
using DatingApp.Api.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DatingApp.Api.Controllers
{
    [ServiceFilter(typeof(LogUserActivity))]
    [Authorize]
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly IBaseRepository _baseRepository;
        private readonly IMapper _mapper;
        public UsersController(IBaseRepository baseRepository, IMapper mapper)
        {
            _mapper = mapper;
            _baseRepository = baseRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers([FromQuery] UserParams userParams)
        {
            int currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var userFromRepo = await _baseRepository.GetUserAsync(currentUserId);

            userParams.UserId = userFromRepo.Id;
            
            if(string.IsNullOrEmpty(userParams.Gender))
                userParams.Gender = userFromRepo.Gender == "male" ? "female" : "male";

            var users = await _baseRepository.GetUsersAsync(userParams);

            var usersToDisplay = _mapper.Map<IEnumerable<UserForList>>(users);

            Response.AddPagination(users.CurrentPage, users.TotalPage, 
                users.TotalItems, users.PageSize);

            return Ok(usersToDisplay);
        }

        [HttpGet("{id}", Name="GetUser")]
        public async Task<IActionResult> GetUser(int id)
        {
            var user = await _baseRepository.GetUserAsync(id);
            var userToDisplay = _mapper.Map<UserForDetails>(user);
            return Ok(userToDisplay);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUser(int id, [FromBody]UserForUpdate userForUpdate)
        {
            if(id != Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var userFromRepo = await _baseRepository.GetUserAsync(id);
            _mapper.Map(userForUpdate, userFromRepo);

            if(await _baseRepository.SaveAll())
                return NoContent();

            throw new Exception("There was a problem with save changes");
        }

        [HttpPost("{id}/like/{recipientId}")]
        public async Task<IActionResult> LikeUser(int id, int recipientId) {
            if(id != Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var likeEntity = await _baseRepository.GetLike(id, recipientId);
            if(likeEntity != null)
                return BadRequest("User already likes this user");

            if(await _baseRepository.GetUserAsync(recipientId) == null)
                return NotFound();

            likeEntity = new Like {
                LikeeId = recipientId,
                LikerId = id
            };

            _baseRepository.Add<Like>(likeEntity);

            if(await _baseRepository.SaveAll())
                return Ok();

            return BadRequest("Failed user like");
        }
    }
}