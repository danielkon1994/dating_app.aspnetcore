using System.Collections.Generic;
using System.Text;
using DatingApp.Api.Models;
using Newtonsoft.Json;

namespace DatingApp.Api.Data
{
    public class SeedData
    {
        private readonly DataContext _context;
        public SeedData(DataContext context)
        {
            _context = context;
        }

        public void SeedUsers() {
            _context.Users.RemoveRange(_context.Users);
            _context.SaveChanges();

            // seed users
            var userSeedDataText = System.IO.File.ReadAllText("./Data/UserSeedData.json");
            var userDataJson = JsonConvert.DeserializeObject<List<User>>(userSeedDataText);
            foreach(var user in userDataJson) {
                user.Username = user.Username.ToLower();
                
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash("password", out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;

                _context.Users.Add(user);
            }

            _context.SaveChanges();
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using(var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }
    }
}