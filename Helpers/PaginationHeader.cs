namespace DatingApp.Api.Helpers
{
    public class PaginationHeader
    {
        public int CurrentPage { get; set; }
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
        public int PageSize { get; set; }

        public PaginationHeader(int currentPage, int totalPage,
            int totalItems, int pageSize)
        {
            this.CurrentPage = currentPage;
            this.TotalPage = totalPage;
            this.TotalItems = totalItems;
            this.PageSize = pageSize;
        }
    }
}