using System.Linq;
using AutoMapper;
using DatingApp.Api.Dto;
using DatingApp.Api.Models;

namespace DatingApp.Api.Helpers
{
    public class AutoMapperProfiler : Profile
    {
        public AutoMapperProfiler()
        {
            CreateMap<User, UserForList>()
                .ForMember(x => x.Age, d => d.ResolveUsing(x => x.DateOfBirth.CalculateAge()))
                .ForMember(x => x.PhotoUrl, d => d.MapFrom(x => x.Photos.FirstOrDefault(p => p.IsMain == true).Url));
            
            CreateMap<User, UserForDetails>()
                .ForMember(x => x.Age, d => d.ResolveUsing(x => x.DateOfBirth.CalculateAge()))
                .ForMember(x => x.PhotoUrl, d => d.MapFrom(x => x.Photos.FirstOrDefault(p => p.IsMain == true).Url));
                
            CreateMap<Photo, PhotoForDetail>();

            CreateMap<UserForUpdate, User>();

            CreateMap<PhotoForAddDto, Photo>();

            CreateMap<Photo, PhotoForReturnDto>();

            CreateMap<UserForRegister, User>();

            CreateMap<MessageForCreationDto, Message>().ReverseMap();

            CreateMap<Message, MessageForReturnDto>()
                .ForMember(m => m.SenderPhotoUrl, o => o.MapFrom(s => s.Sender.Photos.FirstOrDefault(i => i.IsMain).Url))
                .ForMember(m => m.RecipientPhotoUrl, o => o.MapFrom(s => s.Recipient.Photos.FirstOrDefault(i => i.IsMain).Url));
        }
    }
}