using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.Api.Helpers
{
    public class PagedList<T> : List<T>
    {
        public int CurrentPage { get; set; }        
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
        public int PageSize { get; set; }

        public PagedList(List<T> items, int currentPage,
            int totalItems, int pageSize)
        {
            this.CurrentPage = currentPage;
            this.TotalPage = (int)Math.Ceiling(totalItems / (double)pageSize);
            this.TotalItems = totalItems;
            this.PageSize = pageSize;
            this.AddRange(items);   
        }

        public static async Task<PagedList<T>> CreateAsync(IQueryable<T> source, int pageNumber,
            int pageSize)
        {
            int count = await source.CountAsync();
            var items = await source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
            return new PagedList<T>(items, pageNumber, count, pageSize);
        }
    }
}