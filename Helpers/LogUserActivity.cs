using System;
using System.Security.Claims;
using System.Threading.Tasks;
using DatingApp.Api.Repository;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace DatingApp.Api.Helpers
{
    public class LogUserActivity : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var resultContext = await next();

            int userId = int.Parse(resultContext.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var baseRepository = resultContext.HttpContext.RequestServices.GetService<IBaseRepository>();
            var userFromRepo = await baseRepository.GetUserAsync(userId);
            userFromRepo.LastActive = DateTime.Now;

            await baseRepository.SaveAll();
        }
    }
}