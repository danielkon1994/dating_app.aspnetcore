using System;
using System.ComponentModel.DataAnnotations;

namespace DatingApp.Api.Dto
{
    public class UserForRegister
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [MinLength(4, ErrorMessage="Password must have minimum {1} characters.")]
        public string Password { get; set; }

        [Required]
        public string Gender { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [Required]
        public string KnownAs { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        public DateTime LastActive { get; set; }

        public UserForRegister()
        {
            Created = DateTime.Now;
            LastActive = DateTime.Now;
        }
    }
}