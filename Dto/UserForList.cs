using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DatingApp.Api.Dto
{
    public class UserForList
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string Gender { get; set; }

        public int Age { get; set; }

        public string KnownAs { get; set; }

        public DateTime Created { get; set; }

        public DateTime LastActive { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string PhotoUrl { get; set; }

        public ICollection<PhotoForDetail> Photos { get; set; }

        public UserForList()
        {
            Photos = new Collection<PhotoForDetail>();
        }
    }
}