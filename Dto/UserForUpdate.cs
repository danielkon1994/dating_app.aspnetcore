using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DatingApp.Api.Dto
{
    public class UserForUpdate
    {
        public string Introduction { get; set; }

        public string LookingFor { get; set; }

        public string Interests { get; set; }

        public string City { get; set; }

        public string Country { get; set; }
    }
}